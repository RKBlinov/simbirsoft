import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HTML {
    public static void main(String[] args) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        String site = "https://www.simbirsoft.com/";
        String s;
        String t = "";
        Document document = Jsoup.connect(site).get();
        downloadHtmlToFile(site);

        for (Element element : document.getAllElements()) {
            s = element.text();
            t += s + " ";
        }
        String text = t;
        String[] words = text.split("[- .,@!?�:;()<>=/��\\n\\t\\r\\[\\]\"']+");
        for (String word : words) {
            list.add(word);
        }
        list.remove("");

        HashMap<String, Integer> hm = new HashMap<>();
        Integer am;
        for (String i : list) {
            am = hm.get(i);
            hm.put(i, am == null ? 1 : am + 1);
        }
        statistic(hm);
    }

    public static void statistic(HashMap<String, Integer> hashMap) {
        System.out.println("���������� �� ��������� ������");
        System.out.println("-------------------------------");
        for (Map.Entry entry : hashMap.entrySet()) {
            String s = "���";
            if ((int) entry.getValue() % 10 == 2 || (int) entry.getValue() % 10 == 3 || (int) entry.getValue() % 10 == 4
                    && (int) entry.getValue() % 100 > 19) {
                s = "����";
            }
            System.out.println(entry.getKey() + " ����������� " + entry.getValue() + " " + s);
        }
    }

    public static void downloadHtmlToFile(String site) {
        int count;
        byte[] buff = new byte[64];
        InputStream is;
        OutputStream out;

        try {
            is = new URL(site).openStream();
            out = new FileOutputStream("D:/index.html");

            while ((count = is.read(buff)) != -1) {
                out.write(buff, 0, count);
            }
        } catch (IOException e) {
            e.getMessage();
        }
    }
}
